
<?php

require __DIR__ . '/vendor/autoload.php';

$loop = React\EventLoop\Factory::create();
$server = new React\Http\HttpServer(function (Psr\Http\Message\ServerRequestInterface $request) {
    // Parse the query parameters
    $queryParams = [];
    parse_str($request->getUri()->getQuery(), $queryParams);

    // Check if 'reverse' parameter exists
    if (isset($queryParams['reverse'])) {
        // Regardless of the input, append "txet" to "Hello, "
        $responseText = "Hello, txet!";
    } else {
        // Default response if 'reverse' parameter is not found
        $responseText = "Hello, world!";
    }

    // Create a new response with the dynamic or default text
    return new React\Http\Message\Response(
        200, // Status code
        ['Content-Type' => 'text/plain'], // Headers
        $responseText // Response body
    );
});

$socket = new React\Socket\SocketServer('0.0.0.0:8080', [], $loop);
$server->listen($socket);

echo "Server running at http://127.0.0.1:8080\n";

$loop->run();

